## Ubuntu 16.10 Yakkety

#### Milestones

* 2017-03-26 - 3.6 GB - Base OS
* 2017-03-26 - 3.9 GB - Copied /mnt/storage files
* 2017-03-26 - 4.0 GB - Finished step 2, firefox
* 2017-03-26 - 4.3 GB - Installed multimedia apps
* 2017-03-26 - 5.1 GB - Installed developer tools
* 2017-03-27 - 5.2 GB - Reboot
* 2017-03-27 - 5.4 GB - Installed misc apps, chrome
* 2017-03-28 - 5.9 GB - Created Python env (base-3x)
* 2017-03-30 - 6.3 GB - Installed Pinyin input support
* 2017-03-30 - 6.4 GB - Installed Pascal
* 2017-04-29 - 6.9 GB - Reboot
* 2017-05-29 - 7.4 GB - transmission-cli
* 2017-05-30 - 7.4 GB - tremc
* 2017-10-05 - 8.3 GB - java
* 2017-11-13 - 9.6 GB - Reboot

### Step 1 - Create partitions

```
 50 GB - /mnt/win10
       - (Extended partition)
  4 GB - <swap>
 15 GB - ubuntu
 15 GB - arch
154 GB - /mnt/storage
```

### Step 2 - Apply initial configuration

* Make directories
  ```bash
  sudo mkdir /mnt/{arch,homeStorage,storage,tmp,win10}
  sudo chown ian:ian /mnt/{arch,homeStorage,storage,tmp,win10}
  mkdir -p /mnt/storage/work/{node_env,python_env,ruby_env}
  ```
* Update fstab (see repo file)
* Make desktop shortcuts
  ```bash
  ln -s -t ~/Desktop /mnt/storage/work/music_related/song_picks.txt
  ln -s -t ~/Desktop /mnt/storage/work/misc_docs/tdl.txt
  ln -s -t ~/Desktop /mnt/storage/work/student_advisement/schedule/2017-spring.md
  ln -s -t ~/Desktop /mnt/storage/work/student_advisement/calendar/2017-spring-calendar.pdf
  ```
* Make home shortcuts
  ```bash
  ln -s -t . /mnt/storage/work/classwork
  ln -s -t . /mnt/storage/work/code
  ln -s -t . /mnt/storage/work/classwork/csc-420
  ln -s -t . /mnt/storage/work/student_advisement/schedule/other_people
  ln -s -t . /mnt/storage/work/misc_docs/zh_notes
  ```
* Delete unwanted directories
  ```bash
  rm -r ~/{Documents,Downloads,Music,Pictures,Public,Templates,Videos,examples.desktop}
  ```
* Fix dimming
  ```bash
  # sudo vi /etc/default/grub
  # Set: GRUB_CMDLINE_LINUX_DEFAULT="quiet splash acpi_backlight=vendor"
  sudo update-grub
  ```
* Shorten grub timeout
  ```bash
  vi /etc/default/grub
  # Delete lines about hidden timeout
  # Set: GRUB_TIMEOUT=2
  ```
### Step 3 - Install applications

* Uninstalled
  * thunderbird
  * baobab
  * rhythmbox
  * totem
* Updated
	* firefox
* Browsers
  * GoogleTalk video plugin for Firefox (installed via .deb)
    ```bash
    # Firefox requires these OS packages:
    sudo apt-get install -y libpango1.0-0 libpangox-1.0-0
    ```
  * google-chrome-stable (via .deb file)
    ```bash
    # Chrome requires this OS package:
    sudo apt-get install -y libappindicator1
    ```
  * Flash plugin for firefox
    * Utilizes chromium-browser's flash plugin
    * `sudo apt-get install -y browser-plugin-freshplayer-pepperflash`
* Multimedia
  * vlc
  * gstreamer1.0-libav
  * gstreamer1.0-plugins-ugly
  * puddletag
  * audacity
  * imagemagick (pre-installed)
  * inkscape
  * gimp
  * soundconverter
  * visualboyadvance-gtk (run with gvba command)
* Unity
  * indicator-multiload
  * psensor (cpu temperature icon)
  * compizconfig-settings-manager (run with ccsm command)
  * alarm-clock-applet
* Developer tools
  * tmux
  * vim
  * git
  * git-extras
  * mercurial
  * atom
    ```bash
    sudo add-apt-repository ppa:webupd8team/atom
    sudo apt-get update
    sudo apt-get install -y atom
    ```
  * virtualbox (via .deb file)
  * vagrant (via .deb file)
  * slack (via .deb file)
    ```bash
    # Requires these OS packages:
    sudo apt-get install -y libappindicator1 libindicator7
    ```
* Misc
  * curl (pre-installed)
  * tree
  * openssh-server
  * nfs-common (need for personal NAS)
  * gparted
  * smartmontools
  * android-tools-fastboot
  * android-tools-adb
  * unrar

### Step 4 - Setup developer environments

* Python
  ```bash
  sudo apt-get install -y python3-dev python3-pip python-gtk2-dev
  sudo pip3 install -U pip
  sudo pip3 install virtualenvwrapper
  mkdir /mnt/storage/work/python_env
  # add to ~/.bashrc:
  # export WORKON_HOME=/mnt/storage/work/python_env
  # export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
  # source /usr/local/bin/virtualenvwrapper.sh
  # workon base-3x
  # alias nb='jupyter-notebook --no-browser'
  mkvirtualenv python=python3 base-3x
  pip install ipython numpy juptyer matplotlib scipy pandas
  ```
  ```bash
  # For tensor flow environment
  mkvirtualenv tensor
  sudo apt-get install libcupti-dev libhdf5-dev python3-tk
  pip install --upgrade tensorflow keras scikit-learn h5py Pillow ipython matplotlib
  ```
* Ruby
* Node
  ```bash
  # Install node/npm via nvm
  # Follow instructions on: https://github.com/creationix/nvm
  # Don't use sudo
  curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.0/install.sh | bash
  mv /home/ian/.nvm /mnt/storage/work/node_env
  # Update ~/.bashrc
  # Don't forget the --no-use on the 2nd line
  # export NVM_DIR="/mnt/storage/work/node_env"
  # [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" --no-use # This loads nvm
  nvm install 6
  ```
* C++ / Qt
  ```bash
  # Download offline Qt installer from http://qt.io
  # Run installer
  # Take note of install path: /opt/QtX.Y.Z
  sudo apt-get install -y libgl1-mesa-dev
  # Add to ~/.bashrc:
  # PATH=/opt/QtX.Y.Z/X.Y/gcc_64/bin:$PATH
  # export PATH
  # LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/opt/QtX.Y.Z/X.Y/gcc_64/lib"
  # export LD_LIBRARY_PATH
  ```
* Golang
  ```bash
  wget https://storage.googleapis.com/golang/go1.8.linux-amd64.tar.gz
  sha256sum go1.8.linux-amd64.tar.gz | grep 53ab94104ee3923e228a2cb2116e5e462ad3ebaeea06ff04463479d7f12d27ca
  tar xvf go1.8.linux-amd64.tar.gz
  sudo chown -R root:root ./go
  sudo mv go /usr/local
  ```
  Add the following to ~/.profile
  ```bash
  # Golang related
  # Added 2017-02-22
  export GOPATH=$HOME/work
  export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin
  ```
* Pascal
  ```bash
  sudo apt-get install -y fp-compiler
  ```
* Java
  ```bash
  sudo add-apt-repository ppa:webupd8team/java
  sudo apt-get update
  sudo apt-get install -y oracle-java8-installer oracle-java8-set-default
  ```
* Processing
  * Processing environment/IDE
    ```bash
    sudo mkdir /opt/processing
    cd /opt/processing
    # Download tar installation files
    # from: https://processing.org/download/
    # or run:
    sudo wget http://download.processing.org/processing-3.3.6-linux64.tgz

    sudo tar xzfv processing-3.3.6-linux64.tgz
    sudo chown ian:ian processing-3.3.6
    ./processing-3.3.6/install.sh
    ```
  * Processing.py support from the command line
    ```bash
    # Download standalone version of Processing.py
    # from here: http://py.processing.org/tutorials/command-line/
    # or by running:
    wget http://py.processing.org/processing.py-linux64.tgz

    # (Optional) add helpful alias to ~/.bashrc
    alias pyp='java -jar /opt/processing/processing-3.3.6/modes/processing.py-3017-linux64/processing-py.jar'
    ```

### Step 5 - Configure applications

* OS settings
  * Appearance
    * Launcher icon size = 34
    * Auto-hide the launcher
    * Reveal sensitivity = max
    * Enable workspaces
  * Brightness & Lock
    * Turn off screen = 2min
    * Disable lock
  * Language Support
    * Source: http://www.pinyinjoe.com/linux/ubuntu-12-chinese-setup.htm
    * Accept prompt to fix missing languages
    * Click install languages, check simplifed chinese
    * Log out, login
    * Keyboard input method = fcitx
    * Log out, login (fcitx icon should be shown in the system tray)
    * Click fcitx icon, click Text Entry Settings
      * Add Pinyin (fcitx)
    * Type 'Ctrl-Space' and typo a word to confirm that the language works
  * Security & Privacy
    * Disable sending of occasional system info
  * Keyboard
    * Sound/media - disable play, pause, stop, previous, next
    * Launchers - calculator = Pause
    * Custom - `unity-control-center sound` = `Ctrl+`
  * Power
    * Suspend when inactive = don't suspend
    * When lid is closed = do nothing
  * Details
    * Default music/video application = vlc
  * Updates
    * Automatically check for updates = never
    * Also run:
    ```bash
    sudo sed -i 's/NoDisplay=true/NoDisplay=false/g' /etc/xdg/autostart/*.desktop
    gnome-session-properties # requires GUI
    # uncheck updates and other unwanted things
    ```
  * Time & Clock
    * Show date and month
* Nautilus
  ```bash
  # Make unwanted bookmarks removable
  cp nautilus_dirs ~/.config/user-dirs.dirs
  # Remove unwanted bookmarks (via GUI)
  # Add desired bookmarks:
  # /, storage, downloads, music-not_ready, work, classwork, homeStorage
  ```
* Terminal
  * Command - Enable run command as login shell
  * Colors
    ```bash
    # In Edit - Profile Preferences - Colors
    # Text and background = solarized dark
    # Pallete = solarized
    wget https://raw.githubusercontent.com/seebi/dircolors-solarized/master/dircolors.256dark -O ~/.dircolors
    ```
  * Prompt text
    * Edit ~/.bashrc
    * Find three lines that start with 'PS1='
    * Replace 'w' with 'W'
  * Convenient links
    ```bash
    ln -s -t ~ /mnt/storage/work/classwork
    ln -s -t ~ /mnt/storage/work/code
    ```
  * Hosts file (for networking)
    ```bash
    sudo cp hosts /etc/hosts
    ```
* Browsers
  * Firefox
    * General
      * Disable default browser check
      * Homepage = about:blank
      * Downloads = /mnt/storage/downloads
    * Search
      * Default = DuckDuckGo
      * Open: http://mycroftproject.com/, and add:
        * duckduckgo.com - (pre-installed)
        * wikipedia.org - (pre-installed)
        * google.com - (pre-installed)
        * musicbrainz.org - Artist (by HavoCci)
        * allmusic.com - (by naesk)
        * songmeanings.net - Artist (by Mycroft Project)
        * thefreedictionary.com (by Farlex)
        * youtube.com - (by Mycroft Project)
    * Privacy
      * Tracking = enable tracking protection
      * History = custom
        * Enable browsing/download history
        * Disable search/form history
        * Keep cookies = until I close Firefox
        * Enable clear history when Firefox closes
      * Location bar = history, bookmarks (no open tabs)
    * Security - disable remember logins for sites
    * Sync
      * Add Firefox account
      * Sync = only bookmarks
      * Set device name
    * Advanced
      * Enable override auto cache management
      * Limit cache to 50 MB
    * Add-ons
      * Adblock Plus - disable allow non-intrusive ads
      * Firebug
      * New tong wen tang (traditional-simplified chinese converter)
      * NoScript
        * General
          * Disable open permissions menu on hover
          * Disable left click permission toggling
        * Appearance
          * Enable full domain
          * Disable base 2nd domain
          * Notifications - disable show message about blocked scripts
      * VimFX
  * Google Chrome
    * On startup - open specific page (about:blank)
    * Appearance
      * Show home button (about:blank)
      * Always show bookmark bar
    * Search - remove everything but google
    * Privacy
      * Disable
        * Web service for navigation errors
        * Prediction service
        * Auto report detail of security incidents
        * Web service to help resolve spelling errors
        * Auto send usage stats
      * Enable
        * Protect you and your device from dangerous sites
        * Send a 'Do Not Track' request
    * Passwords
      * Disable autofill forms
      * Disable offer to save passwords
    * Downloads - set directory
    * Google cloud print - disable show notification
    * Extensions
      * Remove all Google Docs extensions
      * Install Adblock Plus - disable allow non-intrusive ads
* Multimedia
  * vlc - global hotkeys for play, pause, stop, previous, next
  * puddletag - add pattern: `%artist%/%year% - %album%/$num(%track%,2) %title%`
* Transmission
  * Downloading - save to location = /mnt/storage/downloads
  * Seeding - stop seeding at ratio = 3.0
  * Privacy - encryption mode = require encryption
  * Network - enable use local peer discovery
  * CLI
    ```bash
    # Download apt-get dependency
    sudo apt-get install -y transmission-daemon transmission-cli

    # Download tremc
    sudo cp -r tremc /opt
    cd /opt/tremc
    sudo wget https://raw.githubusercontent.com/louipc/tremc/master/tremc
    sudo chmod +x tremc

    # Make torrent directories
    mkdir -p /mnt/storage/downloads/torrents/{,in}complete

    # Make user account
    sudo useradd debian-transmission -d /mnt/storage/downloads/torrents
    sudo passwd debian-transmission

    # Add to bashrc
    export PATH=$PATH:/opt/tremc

    # Run
    cd /opt/tremc
    ./tremc
    # Expect 10 sec delay
    # Type Ctr-d
    sudo ./script.sh

    # Enable (optional) feature for displaying each peer's country of origin
    sudo add-apt-repository ppa:maxmind/ppa
    sudo apt-get update
    sudo apt-get install -y libgeoip1 libgeoip-dev geoip-bin
    pip install python-geoip

    # Configure magnet link handling in Firefox
    # Under edit > preferences > applications
    # Add action for magnet: /opt/tremc/tremc
    ```
* Unity
  * indicator-multiload
    * Monitored resources = only processor
    * Enable autostart
    * Monitor width = 100 pixels
    * Monitor update interval = 1000 miliseconds
  * psensor - Enable:
    * Launch on startup
    * Hide window
    * Restore position/size
  * ccsm
    * Desktop - Ubuntu Unity Plugin - Switcher
      * Start switcher = Ctrl+Alt+Tab / Shift-Ctrl-Alt-Tab
      * Start switcher for all viewports = Alt+Tab / Shift-Alt-Tab
    * Accessibility - Enhanced zoom desktop
      * Zoom in = Ctrl-1
      * Zoom out = Ctrl-2
    * General - General options - Desktop size = 3 x 2
* Developer tools
  * tmux
    ```bash
    git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
    cp tmux.conf ~/.tmux.conf
    tmux
    # type `Ctrl-b I` to download plugins
    ```
  * vim
    ```bash
    mkdir -p ~/.vim/autoload ~/.vim/bundle
    curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
    git clone git://github.com/altercation/vim-colors-solarized.git ~/.vim/bundle/vim-colors-solarized
    cp vimrc ~/.vimrc
    ```
  * git
    ```bash
    git config --global user.name "Ian S. McBride"
    git config --global user.email ian.s.mcb@gmail.com
    git config --global core.editor vim
    git config --global merge.tool diff
    git config --global push.default simple

    # Github operations from git command
    # https://github.com/ingydotnet/git-hub
    git clone https://github.com/ingydotnet/git-hub ~
    sudo mv git-hub /opt
    # Add to bashrc:
    source /opt/git-hub/.rc
    ```
  * hg
    ```bash
    cp hgrc ~/.hgrc
	 ```
  * atom
    * Settings > Packages > tree-view
    * Enable hide ignored names
    * Enable hide vcs ignored files
  * virtualbox
    * General
      * Default machine folder
      * /mnt/storage/downloads/os_images/virtualbox_files
    * Network - Host-only
      * Add network
      * IP = 192.168.11.1
      * Enable DHCP
        * Address = 192.168.11.100
        * Lower = 192.168.11.101
        * Upper = 192.168.11.254		
  * vagrant
    * Add to ~/.bashrc
      ```bash
      export VAGRANT_HOME=/mnt/storage/downloads/os_images/vagrant_files
      ```
  * slack
    * Linux app - enable leave app running in notification area
    * Language and region - Time zone
    * Sidebar - Enable show all unreads
    * Advanced - Downloads location = /mnt/storage/downloads
