## Ubuntu X.YZ CODENAME

#### Milestones

* DATE-1 - ?.? GB - Base OS
* DATE-2 - ?.? GB - Browsers
* DATE-3 - ?.? GB - Multimedia
* DATE-4 - ?.? GB - Developer tools

### General notes

### Partition table

### Packages

* Uninstalled
* Updated
* Browsers
* Multimedia
* Unity
* Developer tools

### Developer environments

* Python
* Ruby
* Node

### Configuration

* Browsers
* Multimedia
* Unity
* Developer tools
