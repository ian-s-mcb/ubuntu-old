## Ubuntu 17.10 Artful

#### Milestones

* 2017-11-14 - 3.2 GB - Base OS
* 2017-11-14 - 3.4 GB - Apt-get update
* 2017-11-14 - 3.2 GB - Uninstall unwanted apps
* 2017-11-14 - 4.5 GB - Multimedia, dev tools, misc apps
* 2017-11-14 - 5.0 GB - Python base-3x env
* 2017-11-14 - 5.5 GB - Atom
* 2017-11-14 - 5.6 GB - Sunpinyin
* 2017-11-14 - 5.8 GB - Chrome
* 2017-11-14 - 5.7 GB - Reboot
* 2017-11-14 - 5.8 GB - Install, uninstall gnome shotwell, photos
* 2017-11-14 - 5.8 GB - Gnome alarms photos
* 2017-11-14 - 5.8 GB - Gnome shell extension system monitor
* 2017-11-21 - 5.9 GB - Tmuxp
* 2017-11-26 - 6.0 GB - Reboot
* 2017-11-26 - 6.0 GB - System nodejs
* 2017-12-06 - 6.1 GB - Reboot
* 2017-12-06 - 6.1 GB - Base-16 colors
* 2018-01-01 - 6.4 GB - Vim-gtk
* 2018-01-15 - 6.7 GB - Mpd, ncmpcpp
* 2018-03-31 - 6.8 GB - Reboot
* 2018-03-31 - 6.8 GB - ffmpeg
* 2018-05-11 - 6.9 GB - split2flac

#### In progress fixes

* Youtube playback isn't resumed after reopening video
  * Problem occurs in Firefox (v.57), Chrome (v.62)
  * Tried disabling all extensions, restarted

### Step 1 - Create partitions

```
 50 GB - /mnt/win10
       - (Extended partition)
  4 GB - <swap>
 15 GB - ubuntu
 15 GB - arch
154 GB - /mnt/storage
```

### Step 2 - Apply initial configuration

* Make directories
  ```bash
  sudo mkdir /mnt/{arch,homeStorage,storage,tmp,win10}
  sudo chown ian:ian /mnt/{arch,homeStorage,storage,tmp,win10}
  mkdir -p /mnt/storage/work/{node_env,python_env,ruby_env}
  ```
* Update fstab (see repo file)
* Make desktop shortcuts
  ```bash
  ln -s -t ~/Desktop /mnt/storage/work/music_related/song_picks.txt
  ln -s -t ~/Desktop /mnt/storage/work/misc_docs/tdl.txt
  ln -s -t ~/Desktop /mnt/storage/work/student_advisement/schedule/2018-spring.md
  ln -s -t ~/Desktop /mnt/storage/work/student_advisement/calendar/2018-spring-calendar.pdf
  ```
* Make home shortcuts
  ```bash
  ln -s -t ~ /mnt/storage/work/classwork
  ln -s -t ~ /mnt/storage/work/code
  ln -s -t ~ /mnt/storage/work/misc_docs/zh_notes
  ln -s -t ~ /mnt/storage/install/ubuntu_17.10_artful
  mv ~/ubuntu_17.10_artful ~/install_notes
  ```
* Copy my personal setup scripts
  ```bash
  cp -r ~/install_notes/setup_scripts ~/code
  ```
* Delete unwanted directories
  ```bash
  rm -r ~/{Documents,Downloads,Music,Pictures,Public,Templates,Videos,examples.desktop}
  ```
* Fix dimming
  ```bash
  # sudo vi /etc/default/grub
  # Set: GRUB_CMDLINE_LINUX_DEFAULT="quiet splash acpi_backlight=vendor"
  sudo update-grub
  ```
* Shorten grub timeout
  ```bash
  vi /etc/default/grub
  # Delete lines about hidden timeout
  # Set: GRUB_TIMEOUT=2
  sudo update-grub
  ```
* Prevent suspend when closing laptop lid
  ```bash
  # Add line to /etc/systemd/logind.conf
  HandleLidSwitch=ignore
  # Reboot
  ```
* Fix abbreviated gnome shell top bar
  ```bash
  gsettings set org.gnome.desktop.interface clock-show-date true
  gsettings set org.gnome.desktop.interface show-battery-percentage true
  ```
* Hide trash icon on desktop
  ```bash
  gsettings set org.gnome.nautilus.desktop trash-icon-visible false
  ```

### Step 3 - Install applications

* Uninstalled
  * thunderbird
  * baobab
  * rhythmbox
  * totem
  * transmission-gtk
  * Run:
    ```bash
    sudo apt-get purge -y thunderbird baobab rhythmbox totem transmission-gtk
    ```
* Updated
  * firefox (already up-to-date, ver: 56.0+build6-0ubuntu1)
  * gnome-control-center
    * Bugfix for duplicate entries in notifications settings
    * Original version: 1:3.26.1-0ubuntu4
    * New version: 1:3.26.1-0ubuntu5
* Browsers
  * google-chrome-stable (via .deb file)
    ```bash
    # Chrome requires this OS package:
    sudo apt-get install -y libappindicator1
    ```
* Multimedia
  * vlc
  * gstreamer1.0-libav
  * gstreamer1.0-plugins-ugly
  * puddletag
  * audacity
  * imagemagick (pre-installed)
  * inkscape
  * gimp
  * soundconverter
  * visualboyadvance-gtk (run with gvba command)
  * transmission-daemon
  * transmission-cli
  * ncmpcpp
  * ffmpeg
  * libavcodec-extra
  * libav-toolsffmpeg
  * libavcodec-extra
  * libav-tools
  * cuetools
  * shntool
  * split2flac (via git)
* Developer tools
  * build-essential
  * tmux
  * vim-gtk (comes with system clipboard support)
  * git
  * git-extras
  * mercurial
  * atom
    ```bash
    sudo add-apt-repository ppa:webupd8team/atom
    sudo apt-get update
    sudo apt-get install -y atom
    ```
  * virtualbox (via .deb file)
    ```bash
    # Requires these OS packages:
    sudo apt-get install -y build-essential libqt5opengl5 libqt5printsupport5
    ```
  * vagrant (via .deb file)
  * slack (via .deb file)
    ```bash
    # Requires these OS packages:
    sudo apt-get install -y libappindicator1 libindicator7
    ```
* Misc
  * curl
  * tree
  * openssh-server
  * nfs-common (need for personal NAS)
  * gparted
  * smartmontools
  * android-tools-fastboot
  * android-tools-adb
  * unrar

### Step 4 - Setup developer environments

* Python
  ```bash
  sudo apt-get install -y python3-dev python3-pip python-gtk2-dev
  sudo pip3 install -U pip
  sudo pip3 install virtualenvwrapper

  mkdir /mnt/storage/work/python_env

  # add to ~/.bashrc:
  # export WORKON_HOME=/mnt/storage/work/python_env
  # export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
  # source /usr/local/bin/virtualenvwrapper.sh
  # workon base-3x
  # alias nb='jupyter-notebook --no-browser'

  mkvirtualenv python=python3 base-3x
  pip install ipython numpy jupyter matplotlib scipy pandas tmuxp
  deactivate

  # Delete the Python environment lines in ~/.bashrc
  # Reason: instead of loading the virtualenvwrappper and base-3x in
  # every shell, the code/setup_scripts/setup_py file will be sourced
  # when necessary. This is better because loading this environment
  # slows down the opening of new shells.
  ```
  ```bash
  # For tensor flow environment
  mkvirtualenv tensor
  sudo apt-get install libcupti-dev libhdf5-dev python3-tk
  pip install --upgrade tensorflow keras scikit-learn h5py Pillow ipython matplotlib
  ```
* Ruby
* Node
  ```bash
  # Install node/npm via nvm
  # Follow instructions on: https://github.com/creationix/nvm
  # Don't use sudo
  curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.0/install.sh | bash
  mv /home/ian/.nvm /mnt/storage/work/node_env
  # Update ~/.bashrc
  # Don't forget the --no-use on the 2nd line
  # export NVM_DIR="/mnt/storage/work/node_env"
  # [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" --no-use # This loads nvm
  nvm install 6
  ```
  ```bash
  # Install a system node (instead of nvm)
  curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
  sudo apt-get install -y nodejs
  ```
* C++ / Qt
  ```bash
  # Download offline Qt installer from http://qt.io
  # Run installer
  # Take note of install path: /opt/QtX.Y.Z
  sudo apt-get install -y libgl1-mesa-dev
  # Add to ~/.bashrc:
  # PATH=/opt/QtX.Y.Z/X.Y/gcc_64/bin:$PATH
  # export PATH
  # LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/opt/QtX.Y.Z/X.Y/gcc_64/lib"
  # export LD_LIBRARY_PATH
  ```
* Golang
  ```bash
  wget https://storage.googleapis.com/golang/go1.8.linux-amd64.tar.gz
  sha256sum go1.8.linux-amd64.tar.gz | grep 53ab94104ee3923e228a2cb2116e5e462ad3ebaeea06ff04463479d7f12d27ca
  tar xvf go1.8.linux-amd64.tar.gz
  sudo chown -R root:root ./go
  sudo mv go /usr/local
  ```
  Add the following to ~/.profile
  ```bash
  # Golang related
  # Added 2017-02-22
  export GOPATH=$HOME/work
  export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin
  ```
* Pascal
  ```bash
  sudo apt-get install -y fp-compiler
  ```
* Java
  ```bash
  sudo add-apt-repository ppa:webupd8team/java
  sudo apt-get update
  sudo apt-get install -y oracle-java8-installer oracle-java8-set-default
  ```
* Processing
  * Processing environment/IDE
    ```bash
    sudo mkdir /opt/processing
    cd /opt/processing
    # Download tar installation files
    # from: https://processing.org/download/
    # or run:
    sudo wget http://download.processing.org/processing-3.3.6-linux64.tgz

    sudo tar xzfv processing-3.3.6-linux64.tgz
    sudo chown ian:ian processing-3.3.6
    ./processing-3.3.6/install.sh
    ```
  * Processing.py support from the command line
    ```bash
    # Download standalone version of Processing.py
    # from here: http://py.processing.org/tutorials/command-line/
    # or by running:
    wget http://py.processing.org/processing.py-linux64.tgz

    # (Optional) add helpful alias to ~/.bashrc
    alias pyp='java -jar /opt/processing/processing-3.3.6/modes/processing.py-3017-linux64/processing-py.jar'
    ```

### Step 5 - Configure applications

* OS settings
  * Dock
    * Enable auto-hide
    * Icon size = 34
  * Notifications
    * Disable ubuntu software
  * Search
    * Disable ubuntu software
  * Region, language
    * Click manage installed languages - accept prompt to fix missing languages
    * Run: 
      ```bash
      sudo apt-get install -y ibus-pinyin ibus-sunpinyin
      ibus restart
      gnome-control-center region # Requires GUI
      # Under input sources, click plus, click ..., click other,
      # Type pinyin, choose sunpinyin
      # Confirm input works by type Super+Space, then something Chinese
      ```
  * Online accounts
    * Add google account
    * Use for only calendar
  * Privacy
    * Enable
      * Screen lock
      * Usage history
    * Disable
      * Location services
      * Purge trash, temp files
      * Network connectivity checking (aka captive portal detection)
    * Manual - problem reporting
  * Power
    * Blank screen = 2 min
  * Devices
    * Keyboard
      * Launchers
        * Calculator = Pause
        * Settings = Ctrl+`
      * Sound, media - eject = disabled
    * Mouse, touchpad - disable natural scrolling
    * Displays - night light - manual (17:00 - 5:00)
    * Removable media - enable never prompt
  * Details
    * Date, time
      * Time zone = Taipei
      * Time format = AM/PM
    * Users - set icon
  * Other settings (not found in the OS settings GUI)
    * Autostart on login
      ```bash
      # Makes system services visible in the autostart GUI
      sudo sed -i 's/NoDisplay=true/NoDisplay=false/g' /etc/xdg/autostart/*.desktop
      # Adds my apps to autostart list
      cp -r ~/install_notes/autostart/* ~/.config/autostart
      ```
    * Updates
      * Run: software-properties-gtk (requires GUI)
      * Automatically check for updates = never
      * When there are security updates = display immediately
      * When there are other updates = display every two weeks
* Nautilus
  ```bash
  # Make unwanted bookmarks removable
  cp ~/install_notes/nautilus_dirs ~/.config/user-dirs.dirs
  # Remove unwanted bookmarks (via GUI)
  # Add desired bookmarks:
  # /, storage, downloads, music-not_ready, work, classwork, code, homeStorage
  ```
* Terminal
  * Command - Enable run command as login shell
  * Colors
    ```bash
    # Use base16 (the color architecture)
    # https://github.com/chriskempson/base16

    # Install python build tool for base16
    # https://github.com/InspectorMustache/base16-builder-python
    mkvirtualenv tmp-base16
    pip install pybase16-builder
    mkdir ~/tmp-base16_files
    cd ~/tmp-base16_files
    pybase16 update

    # Install onedark
    pybase16 build -t gnome-terminal -s onedark -o onedark
    chmod +x onedark/gnome-terminal/color-scripts/base16-onedark-256.sh
    ./onedark/gnome-terminal/color-scripts/base16-onedark-256.sh

    # Install one-light
    pybase16 build -t gnome-terminal -s one-light -o one-light
    chmod +x one-light/gnome-terminal/color-scripts/base16-one-light-256.sh
    ./one-light/gnome-terminal/color-scripts/base16-one-light-256.sh

    # Set darkone to default
    # Gnome terminal - Preferences - Profiles
    # Profile used launching a new terminal = Base 16 OneDark 256

    # Clean up
    cd ~
    rm -rf tmp-base16_files/
    deactivate
    rmvirtualenv tmp-base16

    # For file extension colors when running ls
    # https://github.com/trapd00r/LS_COLORS
    wget https://raw.github.com/trapd00r/LS_COLORS/master/LS_COLORS -O ~/.dircolors
    ```
  * Prompt text
    * Edit ~/.bashrc
    * Find three lines that start with 'PS1='
    * Replace 'w' with 'W'
  * Hosts file (for networking)
    ```bash
    sudo cp ~/install_notes/hosts /etc/hosts
    ```
* Browsers
  * Firefox
    * Fix magnet, apt link handling
      * Go to: about:config
      * Right click, create two new booleans with the following values:
      * network.protocol-handler.expose.magnet = false
      * network.protocol-handler.expose.apt = false
    * General
      * Disable default browser check
      * When firefox start = show home page
      * Homepage = about:blank
      * Downloads = /mnt/storage/downloads
    * Search
      * Add search bar in toolbar
      * Default = DuckDuckGo
      * Enable provide search suggestions
      * Disable show search suggestions in address bar results
      * Open: http://mycroftproject.com/, and add:
        * duckduckgo.com - (pre-installed)
        * wikipedia.org - (pre-installed)
        * thefreedictionary.com (by Farlex)
        * google.com - (pre-installed)
        * musicbrainz.org - Artist (by visiting website)
        * allmusic.com - (by naesk)
        * songmeanings.net - Artist (by Mycroft Project)
        * youtube.com - (by Mycroft Project)
    * Privacy, security
      * Disable remember logins, passwords for websites
      * History = custom
        * Enable browsing/download history
        * Disable search/form history
        * Keep cookies = until I close Firefox
        * Enable clear history when Firefox closes
      * Address bar = history, bookmarks (no open tabs)
      * Tracking protection
        * Use tracking protection = always
        * Send do not track signal = always
      * Data collection
        * Enable technical, interaction data
        * Disable studies
        * Enable crash reports
    * Firefox account
      * Add Firefox account
      * Sync = only bookmarks
      * Set device name = acer-ian
    * Add-ons
      * uBlock Origin
        * An adblock plus replacement
        * Apply medium mode settings described:
        * https://github.com/gorhill/uBlock/wiki/Blocking-mode:-medium-mode
      * uMatrix (noscript replacement)
      * New tong wen tang (traditional-simplified chinese converter)
      * Vimium
      * New tab homepage
      * Facebook video downloader (keep disabled)
      * Adblock plus (keep disabled)
  * Google Chrome
    * On startup - open specific page (about:blank)
    * Appearance
      * Show home button (about:blank)
      * Always show bookmark bar
    * Search - remove everything but google
    * Privacy
      * Disable
        * Web service for navigation errors
        * Prediction service
        * Auto report detail of security incidents
        * Web service to help resolve spelling errors
        * Auto send usage stats
      * Enable
        * Protect you and your device from dangerous sites
        * Send a 'Do Not Track' request
    * Passwords
      * Disable autofill forms
      * Disable offer to save passwords
    * Downloads - set directory
    * Google cloud print - disable show notification
    * Extensions
      * Remove all Google Docs extensions
      * Vimium
      * uBlock Origin
      * uMatrix
      * Line
* Multimedia
  * vlc
    * Global hotkeys for play, pause, stop, previous, next (no fix needed)
    * Fix duplicate tracks from be enqueued when hitting enter in nautius
      * Under tools - preferences - interface
      * Enable
        * Allow only one instance
        * Enqueue items into playlist in one instance mode
  * puddletag - add pattern: `%artist%/%year% - %album%/$num(%track%,2) %title%`
  * split2flac
    ```bash
    cd
    # Confirm that apt packages are installed (cuetools, shntool)
    git clone https://github.com/ftrvxmtrx/split2flac.git
    sudo mv split2flac /opt
    # Add to ~/.bashrc
    # alias split2flac='/opt/split2flac/split2flac'
    ```
* Transmission CLI (tremc)
    ```bash
    # Download apt-get dependency
    sudo apt-get install -y transmission-daemon transmission-cli

    # Download tremc
    sudo cp -r ~/install_notes/tremc /opt
    cd /opt/tremc
    sudo wget https://raw.githubusercontent.com/louipc/tremc/master/tremc
    sudo chmod +x tremc

    # Make torrent directories
    mkdir -p /mnt/storage/downloads/torrents/{,in}complete

    # Make user account
    sudo systemctl stop transmission-daemon.service
    sudo deluser debian-transmission
    sudo useradd debian-transmission -d /mnt/storage/downloads/torrents
    sudo passwd debian-transmission
    sudo chown -R debian-transmission:debian-transmission /mnt/storage/downloads/torrents

    # Add to bashrc
    export PATH=$PATH:/opt/tremc

    # Run
    cd /opt/tremc
    sudo ./tremc
    # Expect 10 sec delay
    # Type Ctr-d
    sudo ./script.sh

    # Configure magnet link handling in Firefox
    # Under edit > preferences > applications
    # Add action for magnet: /opt/tremc/tremc
    ```
* Developer tools
  * tmux
    ```bash
    git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
    cp ~/install_notes/tmux/tmux.conf ~/.tmux.conf
    tmux
    # type `Ctrl-b I` to download plugins
    # Copy config for the base tmux session (powered by tmuxp)
    mkdir ~/.tmuxp
    cp ~/install_notes/tmux/tmuxp_base.yaml ~/.tmuxp/base.yaml
    ```
  * vim
    ```bash
    mkdir -p ~/.vim/autoload ~/.vim/bundle
    curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
    git clone https://github.com/chriskempson/base16-vim.git ~/.vim/bundle/base16-vim
    cp ~/install_notes/vimrc ~/.vimrc
    ```
  * git
    ```bash
    git config --global user.name "Ian S. McBride"
    git config --global user.email ian.s.mcb@gmail.com
    git config --global core.editor vim
    git config --global merge.tool diff
    git config --global push.default simple

    # Github operations from git command
    # https://github.com/ingydotnet/git-hub
    git clone https://github.com/ingydotnet/git-hub ~/git-hub
    sudo mv ~/git-hub /opt
    # Add to bashrc:
    source /opt/git-hub/.rc
    git hub setup # Requires interaction
    ```
  * hg
    ```bash
    cp ~/install_notes/hgrc ~/.hgrc
	 ```
  * Create ssh key
    ```bash
    ssh-keygen -t rsa -b 4096 -C ian.s.mcb@gmail.com
    # Log into Github, Bitbucket and replace key
    ```
  * atom
    * Settings > Packages > tree-view
    * Enable hide ignored names
    * Enable hide vcs ignored files
  * virtualbox
    * General
      * Default machine folder
      * /mnt/storage/downloads/os_images/virtualbox_files
    * Host network manager
      * Click create
      * IP = 192.168.11.1
      * Enable DHCP
        * Address = 192.168.11.100
        * Lower = 192.168.11.101
        * Upper = 192.168.11.254		
  * vagrant
    * Add to ~/.bashrc
      ```bash
      export VAGRANT_HOME=/mnt/storage/downloads/os_images/vagrant_files
      ```
  * slack
    * Linux app - enable leave app running in notification area
    * Language and region - Time zone
    * Sidebar - Enable show all unreads
    * Advanced - Downloads location = /mnt/storage/downloads
* Gnome Shell custimizations
  * Install system monitor gnome extension
    ```bash
    sudo apt-get install -y gnome-tweak-tool
    gnome-tweak-tool
    # In Workspaces
    # Enable static workspaces
    # Number of workspaces = 6

    # Install dependencies for extension support
    sudo apt-get install -y gir1.2-gtop-2.0 gir1.2-networkmanager-1.0  gir1.2-clutter-1.0 gir1.2-gmenu-3.0

    # Install extension support
    sudo apt-get install chrome-gnome-shell

    # Install extensions by visiting page, pressing toggle button
    # https://extensions.gnome.org/extension/120/system-monitor/
    # https://extensions.gnome.org/extension/1166/extension-update-notifier/
    # https://extensions.gnome.org/extension/16/auto-move-windows/
    # https://extensions.gnome.org/extension/484/workspace-grid/
    # https://extensions.gnome.org/extension/19/user-themes/

    # Label workspaces
    gsettings set org.gnome.desktop.wm.preferences workspace-names "['Terminal', 'WWW', 'Media', 'Social']"
    gsettings set org.gnome.desktop.wm.preferences workspace-names "['Terminal', 'WWW', 'Media', 'Code', 'Social', 'Misc']"
    ```
