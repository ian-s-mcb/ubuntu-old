#!/bin/bash

systemctl start transmission-daemon.service
cp /opt/tremc/settings.json /mnt/storage/downloads/torrents/.config/transmission-daemon/settings.json
invoke-rc.d transmission-daemon reload
/opt/tremc/tremc -f ./tremc.conf
