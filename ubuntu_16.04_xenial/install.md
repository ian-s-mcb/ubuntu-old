## Ubuntu 16.04 Xenial

#### Milestones

* 2016-05-29 - ?.? GB - Base OS
* 2016-07-02 - 7.3 GB - Lost install notes

### Step 1 - Create partitions

```
 4 GB - <swap>
15 GB - /
35 GB - /mnt/win10
65 GB - /mnt/storage
```

### Step 2 - Apply initial configuration

* Update fstab (see repo file)
* Fix dimming
	```bash
	# sudo vi /etc/default/grub
	# Set: GRUB_CMDLINE_LINUX_DEFAULT="quiet splash acpi_backlight=vendor"
	sudo update-grub
	```
* Shorten grub timeout
	```bash
	vi /etc/default/grub
	# Delete lines about hidden timeout
	# Set: GRUB_TIMEOUT=2
	```
### Step 3 - Install applications

* Uninstalled
	* thunderbird
	* baobab
	* rhythmbox
	* totem
* Updated
	* firefox
* Browsers
	* GoogleTalk video plugin for Firefox (installed via .deb)
	* google-chrome-stable (via .deb file)
		```bash
		# Chrome requires this OS package:
		sudo apt-get install -y libappindicator1
		```
	* Flash plugin for firefox
		* Utilizes chromium-browser's flash plugin
		* `sudo apt-get install -y browser-plugin-freshplayer-pepperflash`
* Multimedia
	* vlc
	* gstreamer1.0-libav
	* gstreamer1.0-plugins-ugly
	* puddletag
	* audacity
	* imagemagick (pre-installed)
	* inkscape
	* gimp
	* soundconverter
	* visualboyadvance-gtk (run with gvba command)
* Unity
	* unity-tweak-tool
	* indicator-multiload
	* psensor (cpu temperature icon)
  * compizconfig-settings-manager (run with ccsm command)
* Developer tools
	* tmux
	* vim
	* git
	* git-extras
	* hg
	* atom
		```bash
		sudo add-apt-repository ppa:webupd8team/atom
		sudo apt-get update
		sudo apt-get install -y atom
		```
	* virtualbox (via .deb file)
	* vagrant (via .deb file)
	* slack (via .deb file)
* Misc
	* curl (pre-installed??)
	* tree (pre-installed??)
	* openssh-server
	* nfs-common (need for personal NAS)
	* gparted
	* smartmontools (pre-installed??)
	* android-tools-fastboot
	* android-tools-adb

### Step 4 - Setup developer environments

* Python
  ```bash
  sudo apt-get install -y python3-dev python3-pip
	sudo pip3 install -U pip
	sudo pip3 install virtualenvwrapper
	mkdir /mnt/storage/work/python_env
	# add to ~/.bashrc:
	# export WORKON_HOME=/mnt/storage/work/python_env
	# source /usr/local/bin/virtualenvwrapper.sh
	mkvirtualenv python=python3 base-3x
	pip install ipython numpy juptyer matplotlib scipy pandas
  ```
* Ruby
* Node
  ```bash
  # Install node/npm via nvm
  # Follow instructions on: https://github.com/creationix/nvm
  # Don't use sudo
  curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.0/install.sh | bash
  mv /home/ian/.nvm /mnt/storage/work/node_env
  # Update ~/.bashrc
  # Don't forget the --no-use on the 2nd line
  # export NVM_DIR="/mnt/storage/work/node_env"
  # [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" --no-use # This loads nvm
  nvm install 6
  ```
* C++ / Qt
  ```bash
  # Download offline Qt installer from http://qt.io
  # Run installer
  # Take note of install path: /opt/QtX.Y.Z
  sudo apt-get install -y libgl1-mesa-dev
  # Add to ~/.bashrc:
  # PATH=/opt/QtX.Y.Z/X.Y/gcc_64/bin:$PATH
  # export PATH
  # LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/opt/QtX.Y.Z/X.Y/gcc_64/lib"
  # export LD_LIBRARY_PATH
  ```
* Golang
   ```bash
   wget https://storage.googleapis.com/golang/go1.8.linux-amd64.tar.gz
   sha256sum go1.8.linux-amd64.tar.gz | grep 53ab94104ee3923e228a2cb2116e5e462ad3ebaeea06ff04463479d7f12d27ca
   tar xvf go1.8.linux-amd64.tar.gz
   sudo chown -R root:root ./go
   sudo mv go /usr/local
   ```
   Add the following to ~/.profile
   ```bash
   # Golang related
   # Added 2017-02-22
   export GOPATH=$HOME/work
   export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin
   ```

### Step 5 - Configure applications

* OS settings
  * Appearance
    * Launcher icon size = 34
    * Auto-hide the launcher
    * Reveal sensitivity = max
    * Enable workspaces
  * Brightness & Lock
    * Turn off screen = 2min
    * Disable lock
  * Language Support - TODO
    * See http://www.pinyinjoe.com/linux/ubuntu-12-chinese-setup.htm
  * Security & Privacy
    * Disable recording of file and application usage
  * Text entry - TODO
  * Keyboard
    * Sound/media - disable play, pause, stop, previous, next
    * Launchers - calculator = Pause
    * Custom - `unity-control-center sound` = `Ctrl+`
  * Power
    * Suspend when inactive = don't suspend
    * When power is critically low = power off
    * When lid is closed = do nothing
  * Details
    * Default music/video application = vlc
  * Updates
    * Automatically check for updates = never
    * Also run:
    ```bash
    sudo sed -i 's/NoDisplay=true/NoDisplay=false/g' /etc/xdg/autostart/*.desktop
    gnome-session-properties # requires GUI
    # uncheck updates and other unwanted things
    ```
  * Time & Clock
    * Show date and month
* Terminal
  * Command - Enable run command as login shell
  * Colors
    * Text, bg color = solarized dark
    * Pallete = solarized
    * Run:
      ```bash
      mv ~/.dircolors ~/.dircolors.old
      wget https://raw.githubusercontent.com/seebi/dircolors-solarized/master/dircolors.256dark -O ~/.dircolors
      ```
    * Add to bashrc
      ```bash
      ## enable color support of ls and also add handy aliases
      if [ -x /usr/bin/dircolors ]; then
          test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
          alias ls='ls --color=auto'
          #alias dir='dir --color=auto'
          #alias vdir='vdir --color=auto'
          alias grep='grep --color=auto'
          alias fgrep='fgrep --color=auto'
          alias egrep='egrep --color=auto'
      fi
      ```
* Browsers
  * Firefox
    * General
      * Disable default browser check
      * Homepage = about:blank
      * Downloads = /mnt/storage/downloads
    * Search
      * Default = DuckDuckGo
      * Open: http://mycroftproject.com/, and add:
        * duckduckgo.com - (pre-installed)
        * wikipedia.org - (pre-installed)
        * google.com - (pre-installed)
        * musicbrainz.org - Artist (by HavoCci)
        * allmusic.com - (by naesk)
        * songmeanings.net - Artist (by Mycroft Project)
        * thefreedictionary.com (by Farlex)
        * youtube.com - (by Mycroft Project)
    * Privacy
      * Tracking = enable tracking protection
      * History = custom
        * Enable browsing/download history
        * Disable search/form history
        * Keep cookies = until I close Firefox
        * Enable clear history when Firefox closes
      * Location bar = history, bookmarks (no open tabs)
    * Security - disable remember logins for sites
    * Sync
      * Add Firefox account
      * Sync = only bookmarks
      * Set device name
    * Advanced
      * Enable override auto cache management
      * Limit cache to 50 MB
    * Add-ons
      * Adblock Plus - disable allow non-intrusive ads
      * Firebug
      * NoScript
        * General
          * Disable open permissions menu on hover
          * Disable left click permission toggling
        * Appearance
          * Enable full domain
          * Disable base 2nd domain
        * Notifications - disable show message about blocked scripts
  * Google Chrome
    * On startup - open specific page (about:blank)
    * Appearance
      * Show home button (about:blank)
      * Always show bookmark bar
    * Search - remove everything but google
    * Privacy
      * Disable
        * Web service for navigation errors
        * Prediction service
        * Auto report detail of security incidents
        * Web service to help resolve spelling errors
        * Auto send usage stats
      * Enable
        * Protect you and your device from dangerous sites
        * Send a 'Do Not Track' request
    * Passwords
      * Disable autofill forms
      * Disable offer to save passwords
    * Downloads - set directory
    * Extensions
      * Adblock Plus - disable allow non-intrusive ads
* Multimedia
  * vlc - global hotkeys for play, pause, stop, previous, next
  * puddletag - add pattern: `%artist%/%year% - %album%/$num(%track%,2) %title%`
* Unity
  * indicator-multiload
    * Monitored resources = only processor
    * Enable autostart
    * Monitor width = 100 pixels
    * Monitor update interval = 1000 miliseconds
  * psensor - Enable:
    * Launch on startup
    * Hide window
    * Restore position/size
  * ccsm
    * Desktop - Ubuntu Unity Plugin - Switcher
      * Start switcher = Ctrl+Alt+Tab / Shift-Ctrl-Alt-Tab
      * Start switcher for all viewports = Alt+Tab / Shift-Alt-Tab
    * Accessibility - Enhanced zoom desktop
      * Zoom in = Ctrl-1
      * Zoom out = Ctrl-2
    * General - General options - Desktop size = 3 x 2
* Developer tools
  * tmux
    * Copy tmux.conf to ~/.tmux.conf
    * Run tmux, type `Ctrl-b I` to download plugins
  * vim
    * Run:
    ```bash
    mkdir -p ~/.vim/autoload ~/.vim/bundle
    curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
    ```
    * Copy vimrc to ~/.vimrc
  * git
    * Run:
    ```bash
    git config --global user.name "Ian S. McBride"
    git config --global user.email ian.s.mcb@gmail.com
    git config --global core.editor vim
    git config --global merge.tool diff
    git config --global push.default simple
    ```
  * hg - Copy hgrc to ~/.hgrc
  * atom
    * Settings > Packages > tree-view
      * Enable hide ignored files
      * Enable hide vcs ignored files
  * virtualbox
    * General
      * Default machine folder
      * /mnt/storage/downloads/os_images/virtualbox_files
    * Network - Host-only
      * Add network
      * IP = 192.168.11.1
      * Enable DHCP = address 192.168.11.100
  * vagrant
    * Add to ~/.bashrc
    * export VAGRANT_HOME=/mnt/storage/downloads/os_images/vagrant_files
  * slack
    * Linux app - enable leave app running in notification area
    * Advanced
      * Downloads location = /mnt/storage/downloads
      * Enable all unreads
